This project is no longer maintained.

The contents of this repository are still available in the Git
source code management system.  To see the contents of this
repository before it reached its end of life, please check out the
following commit with
"git checkout 4e21fba46928f5e6e0dea7ca52e3c23826937640"

http://lists.openstack.org/pipermail/openstack-dev/2016-March/090409.html
